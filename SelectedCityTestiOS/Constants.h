//
//  Constants.h
//  Prom
//
//  Created by Александр Сенченков on 15.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import "ErrorObj.h"

#ifndef Constants_h
#define Constants_h

#define SERVER_BASE_URL             @"https://atw-backend.azurewebsites.net"
#define kCityKey                    @"SelectedCity"
#define kFavoriteKey                @"SelectedCityFavorite"
//#define SERVER_IMAGES_BASE_URL      @""


#define COLOR(r,g,b)     [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:1.f]

#define kBorderColorHigh COLOR(120,84,70)
#define kBorderColor COLOR(196,196,196)

#define NULL_TO_NIL(obj)({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj;})
#define NIL_TO_STRING(str) str == nil ? @"" : str;

#define IS_IPHONE       ([[[UIDevice currentDevice] model] hasPrefix: @"iPhone"])
#define IS_IPOD         ([[[UIDevice currentDevice] model] isEqualToString: @"iPod touch"])

#define showError(string) [[SettingsManagers instance] showErrorAlert:string]

#define showSuccess(string) [[SettingsManagers instance] showSuccessAlert:string]

#define NULL_TO_NIL(obj)({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj;})
#define NIL_TO_STRING(str) str == nil ? @"" : str;

#define iOS7             ([[[UIDevice currentDevice] systemVersion] floatValue] == 7) ? YES:NO
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

typedef void (^Callback)(id response,ErrorObj *error);

typedef enum {
    RequestTypePOST,
    RequestTypeGET,
    RequestTypePUT,
    RequestTypeDELETE,
}RequestType;


#endif /* Constants_h */
