//
//  City.m
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "City.h"

@implementation City

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.cityName = @"";
        self.cityID = @"";
        self.countryID = @"";
        self.isFavorite = @"NO";
        self.isSelected = @"NO";
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.cityName = [decoder decodeObjectForKey:@"cityName"];
        self.cityID = [decoder decodeObjectForKey:@"cityID"];
        self.countryID = [decoder decodeObjectForKey:@"countryID"];
        self.isFavorite = [decoder decodeObjectForKey:@"isFavorite"];
        self.isSelected = [decoder decodeObjectForKey:@"isSelected"];
    }
    return self;
}

-(void) fillCityFromDictinary:(NSDictionary *)param
{
    if (param[@"Name"] && ![param[@"Name"] isEqual:[NSNull null]]) {
        
        self.cityName = param[@"Name"];
    }

    if (param[@"Id"] && ![param[@"Id"] isEqual:[NSNull null]]) {
        
        self.cityID = param[@"Id"];
    }
    
    if (param[@"CountryId"] && ![param[@"CountryId"] isEqual:[NSNull null]]) {
        
        self.countryID = param[@"CountryId"];
    }
    

}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.cityName forKey:@"cityName"];
    [encoder encodeObject:self.cityID forKey:@"cityID"];
    [encoder encodeObject:self.countryID forKey:@"countryID"];
    [encoder encodeObject:self.isFavorite forKey:@"isFavorite"];
    [encoder encodeObject:self.isSelected forKey:@"isSelected"];
}




@end
