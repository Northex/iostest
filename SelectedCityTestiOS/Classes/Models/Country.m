//
//  Country.m
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "Country.h"
#import "City.h"

@implementation Country

- (instancetype)init
{
    self = [super init];
    
    if (self) {
    
        self.countryName = @"";
        self.countryID = @"";
        self.citys = [NSMutableArray new];
    }
    return self;
}

-(void) fillCountryFromDictinary:(NSDictionary *)param;
{
    if (param[@"Name"] && ![param[@"Name"] isEqual:[NSNull null]]) {
        
        self.countryName = param[@"Name"];
    }
    
    if (param[@"Id"] && ![param[@"Id"] isEqual:[NSNull null]]) {
        
        self.countryID = param[@"Id"];
    }
    
    if (param[@"Cities"] && ![param[@"Cities"] isEqual:[NSNull null]]) {
        
        NSArray *cities = param[@"Cities"];
        
        if (cities.count !=0) {
            
            [cities enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                City *city = [[City alloc] init];
                [city fillCityFromDictinary:obj];
                [self.citys addObject:city];
                
            }];
        }
    }
}

@end
