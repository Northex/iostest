//
//  Country.h
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Country : NSObject

@property (nonatomic, strong) NSString       *countryName;
@property (nonatomic, strong) NSString       *countryID;
@property (nonatomic, strong) NSMutableArray *citys;

-(void) fillCountryFromDictinary:(NSDictionary *)param;

@end
