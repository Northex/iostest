//
//  City.h
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *cityID;
@property (nonatomic, strong) NSString *countryID;
@property (nonatomic, strong) NSString *isFavorite;
@property (nonatomic, strong) NSString *isSelected;

-(void) fillCityFromDictinary:(NSDictionary *)param;
@end
