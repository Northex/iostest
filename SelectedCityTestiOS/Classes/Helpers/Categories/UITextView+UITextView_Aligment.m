//
//  UITextView+UITextView_Aligment.m
//  TaxiApp
//
//  Created by Admin on 12/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "UITextView+UITextView_Aligment.h"

@implementation UITextView (UITextView_Aligment)


- (void)alignToTop {
    [self addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // When content size change, make sure the content is positioned at the top of the scroll view.
    self.contentOffset = CGPointMake(0.0f, 0.0f);
}

- (void)disableAlginment {
    [self removeObserver:self forKeyPath:@"contentSize"];
}

@end
