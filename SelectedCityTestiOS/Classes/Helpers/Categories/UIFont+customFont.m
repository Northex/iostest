//
//  UIFont+customFont.m
//  SmartHome-iphone
//
//  Created by Jurayev on 8/16/13.
//  Copyright (c) 2013 DS. All rights reserved.
//

#import "UIFont+customFont.h"

#define fontName @"HelveticaNeue-Bold"
#define deffontName @"HelveticaNeue"
#define leftFontName @"HelveticaNeue"

@implementation UIFont (customFont)

+(UIFont *)customFontSize:(float)size {
    return [UIFont fontWithName:fontName size:size];
}

+(UIFont *)leftFontSize:(float)size {
    return [UIFont fontWithName:leftFontName size:size];
}

+(UIFont*)fontBreeSerifRegularWithSize:(float)size{
   return [UIFont fontWithName:@"BreeSerif-Regular" size:size];
}

+(UIFont*)defaultFontWithSize:(float)size{
    return [UIFont fontWithName:deffontName size:size];
}

+(UIFont*)helveticeFontSize:(float)size bold:(BOOL)bold{
    if (bold) {
        return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
    } else{
        return [UIFont fontWithName:@"HelveticaNeue" size:size];
    }
}


@end
