//
//  NSString+MD5Addition.m
//  UIDeviceAddition
//
//  Created by Georg Kitz on 20.08.11.
//  Copyright 2011 Aurora Apps. All rights reserved.
//

#import "NSString+MD5Addition.h"
#import <CommonCrypto/CommonDigest.h>
#import <Foundation/NSString.h>

@implementation NSString (MD5Addition)

- (NSString *)MD5 {
    
    if(self == nil || [self length] == 0)
        return nil;
    
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}

- (NSString *) reverseString {
	NSMutableString *reversedStr;
	NSInteger len = [self length];
    
	// auto released string
	reversedStr = [NSMutableString stringWithCapacity:len];
    
	// quick-and-dirty implementation
	while ( len > 0 )
		[reversedStr appendString:[NSString stringWithFormat:@"%C",[self characterAtIndex:--len]]];
    
	return reversedStr;
}

- (NSString *)hexString
{
    NSUInteger len = [self length];
    unichar *chars = malloc(len * sizeof(unichar));
    [self getCharacters:chars];
    
    NSMutableString *hexString = [[NSMutableString alloc] init];
    
    for (NSUInteger i = 0; i < len; i++ ) {
        [hexString appendFormat:@"%02x", chars[i]]; /*EDITED PER COMMENT BELOW*/
    }
    free(chars);    
    return hexString;
}

- (NSString *)base64String
{
    NSData *data = [NSData dataWithBytes:[self UTF8String] length:[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    NSUInteger length = [data length];
    NSMutableData *mutableData = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    
    uint8_t *input = (uint8_t *)[data bytes];
    uint8_t *output = (uint8_t *)[mutableData mutableBytes];
    
    for (NSUInteger i = 0; i < length; i += 3) {
        NSUInteger value = 0;
        for (NSUInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        static uint8_t const kAFBase64EncodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        
        NSUInteger idx = (i / 3) * 4;
        output[idx + 0] = kAFBase64EncodingTable[(value >> 18) & 0x3F];
        output[idx + 1] = kAFBase64EncodingTable[(value >> 12) & 0x3F];
        output[idx + 2] = (i + 1) < length ? kAFBase64EncodingTable[(value >> 6)  & 0x3F] : '=';
        output[idx + 3] = (i + 2) < length ? kAFBase64EncodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:mutableData encoding:NSASCIIStringEncoding];
}

- (NSString *)xorStringWithKey:(NSString *)key
{
    // Create data object from the string
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get pointer to data to obfuscate
//    char *dataPtr = (char *) [data bytes];
    
    // Get pointer to key data
    char *keyData = (char *) [[key dataUsingEncoding:NSUTF8StringEncoding] bytes];
    
    // Points to each char in sequence in the key
    char *keyPtr = keyData;
    int keyIndex = 0;
    
    // For each character in data, xor with current value in key
    for (int x = 0; x < [data length]; x++) {
        // Replace current character in data with
        // current character xor'd with current key value.
        // Bump each pointer to the next character
//        *dataPtr = *dataPtr++ ^ *keyPtr++;
        
        // If at end of key data, reset count and
        // set key pointer back to start of key value
        if (++keyIndex == [key length])
            keyIndex = 0, keyPtr = keyData;
    }
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

}


- (BOOL)isEmailValid {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isLinkValid {
    
    NSString *urlRegEx = @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    
    return [urlTest evaluateWithObject:self];
}
- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
-(BOOL)isPhoneValid{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    BOOL phoneValidates = [phoneTest evaluateWithObject:self];
    return phoneValidates;
}


//-(NSString *)stringForTag:(NSString*)htmlTag {
//    //    NSRange r;
//    //    NSString *s = [[self copy] autorelease];
//    //    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
//    //        s = [s stringByReplacingCharactersInRange:r withString:@""];
//    //    return [s stringByRemovingNewLinesAndWhitespace];
//    NSError *err;
//    HTMLParser *parser = [[HTMLParser alloc] initWithString:self error:&err];
//    if (!err) {
//        NSString *str = [[[[parser body] findChildTag:htmlTag] contents] trim];
//        return str;
//    }
//    
//    return nil;
//}
//
//- (NSString *)stringTagValueWithName:(NSString *)name
//{
//    NSError *err = NULL;
//    HTMLParser *parser = [[HTMLParser alloc] initWithString:self error:&err];
//    if (!err) {
//        HTMLNode *node = [[parser body] findChildWithAttribute:@"name" matchingName:name allowPartial:YES];
//        NSString *str = [node getAttributeNamed:@"value"];
//        return str;
//    }
//    
//    return nil;
//}

-(NSDictionary*) JSONValue{
    
    NSError *error;
    NSString *str = [NSString stringWithFormat:@"%@",self];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        return nil;
    }
    return dict;
}

+(NSString*) convertDictionaryToString:(NSMutableDictionary*) dict {
    return @"";
//    NSError* error;
//    NSDictionary* tempDict = [dict copy]; // get Dictionary from mutable Dictionary
//    //giving error as it takes dic, array,etc only. not custom object.
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:tempDict
//                                                       options:NSJSONReadingMutableLeaves error:&error];
//    NSString* nsJson=  [[NSString alloc] initWithData:jsonData
//                                             encoding:NSUTF8StringEncoding];
//    return nsJson;
}


@end
