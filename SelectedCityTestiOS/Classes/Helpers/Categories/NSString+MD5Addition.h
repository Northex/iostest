//
//  NSString+MD5Addition.h
//  UIDeviceAddition
//
//  Created by Georg Kitz on 20.08.11.
//  Copyright 2011 Aurora Apps. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class NSString;

@interface NSString (MD5Addition)

- (NSString *)MD5;
- (BOOL)isEmailValid;
- (BOOL)isLinkValid;
- (BOOL)isPhoneValid;
- (NSString *)trim;
-(NSDictionary*) JSONValue;


- (NSString *)reverseString;
- (NSString *)hexString;
- (NSString *)base64String;
- (NSString *)xorStringWithKey:(NSString *)key;
+ (NSString*) convertDictionaryToString:(NSMutableDictionary*) dict;

@end
