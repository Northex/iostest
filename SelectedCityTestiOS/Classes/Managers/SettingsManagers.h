//
//  SettingsManagers.h
//  Prom
//
//  Created by Александр Сенченков on 15.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+MD5Addition.h"




@interface SettingsManagers : NSObject

+ (SettingsManagers *)instance;

- (void)saveObject:(id)obj key:(NSString*)key;
- (id)objForKey:(NSString*)key;

-(void)showErrorAlert:(NSString *)message;
-(void)showSuccessAlert:(NSString *)message;
-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
-(void)clearAllUserData;


@end
