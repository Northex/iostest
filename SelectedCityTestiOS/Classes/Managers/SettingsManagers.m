//
//  SettingsManagers.m
//  Prom
//
//  Created by Александр Сенченков on 15.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import "SettingsManagers.h"

@implementation SettingsManagers

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    
    return self;
}

+ (SettingsManagers *)instance {
    static SettingsManagers *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}

-(void)saveObject:(id)obj key:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] setObject:obj forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(id)objForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)showErrorAlert:(NSString *)message
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Ошибка!"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    [alert addAction:yesButton];
    
    [[self topMostController] presentViewController:alert animated:YES completion:nil];
    
   
}

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    [alert addAction:yesButton];

    [[self topMostController] presentViewController:alert animated:YES completion:nil];
}


-(void)showSuccessAlert:(NSString *)message
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Успех!"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    [alert addAction:yesButton];
    
    [[self topMostController] presentViewController:alert animated:YES completion:nil];
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)clearAllUserData
{
    
}




@end
