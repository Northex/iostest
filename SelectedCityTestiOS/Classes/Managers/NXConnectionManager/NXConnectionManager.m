//
//  IRConnectionManager.m
//  Informator
//
//  Created by Александр Сенченков on 11.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import "NXConnectionManager.h"

@implementation NXConnectionManager

+ (NXConnectionManager *)instance {
    
    static NXConnectionManager *_instance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _instance = [[self alloc] initWithBaseURL:[NSURL URLWithString:SERVER_BASE_URL]];
    });
    
    return _instance;
}

-(id)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:url];
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.responseSerializer.stringEncoding = NSUTF8StringEncoding;
        
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        
//        self.requestSerializer = [AFJSONRequestSerializer serializer];
//        self.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    }
    return self;
}

- (BOOL) canSendRequest
{
    return [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus != AFNetworkReachabilityStatusNotReachable;
}

- (NSError*) noConnectionError
{
    
    
    return [NSError errorWithDomain: @"Internet connection" code: 499 userInfo: @{NSLocalizedDescriptionKey:@"No Internet connection. You are offline."}];
}


-(void)requestPath:(NSString *)path
            params:(NSDictionary *)par
              type:(RequestType)type
          callback:(Callback)callback
{
    self.requestSerializer.HTTPShouldHandleCookies = NO;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:par];
    
    NSMutableDictionary *logDict = [[NSMutableDictionary alloc] init];
    
    logDict[@"params"] = params;
    
    if (path) {
        
        logDict[@"path"] = path;
    }
if ([self canSendRequest])
{
    if (type == RequestTypePOST) {
        
        [self POST:path
        parameters:params
          progress:nil
           success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"Request:%@",logDict);
            NSLog(@"Response:%@",responseObject);
            [self parsing:responseObject error:nil callback:callback];
               
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"Request:%@",logDict);
            NSLog(@"Error:%@",error.userInfo);
            
            [self parsing:nil error:error callback:callback];
        }];
        
        
    } else if(type== RequestTypeGET){
        
        
        [self GET:path
       parameters:params
         progress:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"Request:%@",logDict);
            NSLog(@"Response:%@",responseObject);
            [self parsing:responseObject error:nil callback:callback];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"Request:%@",logDict);
            NSLog(@"Error:%@",error.userInfo);
            [self parsing:nil error:error callback:callback];
            
        }];
        
        
    } else if (type == RequestTypePUT){
        
        [self PUT:path
       parameters:params
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              
            NSLog(@"Request:%@",logDict);
            NSLog(@"Response:%@",responseObject);
            [self parsing:responseObject error:nil callback:callback];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"Request:%@",logDict);
            NSLog(@"Error:%@",error.userInfo);
            [self parsing:nil error:error callback:callback];
            
        }];
        
    } else if(type == RequestTypeDELETE){
        
        [self DELETE:path
          parameters:params
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"Request:%@",logDict);
            NSLog(@"Response:%@",responseObject);
            [self parsing:responseObject error:nil callback:callback];
                 
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"Request:%@",logDict);
            NSLog(@"Error:%@",error.userInfo);
            [self parsing:nil error:error callback:callback];
            
        }];
        
    }
}
    
else
{
    NSLog(@"REQUEST LINK: %@", path);
    
    [self parsing:nil error:[self noConnectionError] callback:callback];
}
    
}


-(void)requestPath:(NSString *)path
            params:(NSDictionary *)par
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
          callback:(Callback)callback
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:par];
    
   // NSMutableDictionary *logDict = [[NSMutableDictionary alloc] init];

    if ([self canSendRequest])
    {
        [self POST:path
        parameters:params constructingBodyWithBlock:block
        progress:nil
           success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
               NSLog(@"Request:%@",params);
               NSLog(@"Response:%@",responseObject);
               [self parsing:responseObject error:nil callback:callback];
        
           } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
               NSLog(@"Request:%@",params);
               NSLog(@"Error:%@",error.userInfo);

               
               [self parsing:nil error:error callback:callback];
           }];
    }
    else
    {
        NSLog(@"REQUEST LINK: %@", path);
        
        [self parsing:nil error:[self noConnectionError] callback:callback];
    }
}

-(void)parsing:(id)response error:(NSError *)error callback:(Callback)callback{
    ErrorObj *resultError= nil;
    
    id resultOb = nil;
    if (response) {

        
        if (![response isKindOfClass:[NSArray class]]) {
            
            NSDictionary *meta = NULL_TO_NIL(response);
            NSInteger code = [NULL_TO_NIL(meta[@"error"]) integerValue];
            
        if (code) {
            resultError = [[ErrorObj alloc] init];
            resultError.title = @"Error";
            resultError.message = NULL_TO_NIL(meta[@"message"]);
            resultError.code = code;
            resultOb = nil;
        } else{
            resultOb = response;
        }
        }
        else
        {
            resultOb = response;
        }

    }
    
    if (error) {
        //NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
      //  NSDictionary *Error = [[NSDictionary alloc] initWithContentsOfFile:ErrorResponse];
        NSDictionary *Error = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:nil];
        
        resultError = [[ErrorObj alloc] init];
        resultError.title = @"Error";
        resultError.message = Error[@"message"];
        resultError.code = [Error[@"error_code"] integerValue];
        resultOb = nil;
    }
    
    if (callback) {
        callback(resultOb,resultError);
    }
    
}




#pragma mark - Defauly methods

- (void)getCountryWithParam:(NSDictionary *)params callback:(Callback) callback;
{
    
    [self requestPath:@"api/countries" params:params type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        if (callback) {
            
            callback(response,error);
            
        }
    }];
}
@end
