//
//  IRConnectionManager.h
//  Informator
//
//  Created by Александр Сенченков on 11.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "Constants.h"


@interface NXConnectionManager : AFHTTPSessionManager

- (NSError *) noConnectionError;

+ (NXConnectionManager *)instance;

//Запрос списка стран
- (void)getCountryWithParam:(NSDictionary *)params callback:(Callback) callback;


@end
