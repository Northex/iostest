//
//  NXFavoriteViewController.m
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "NXFavoriteViewController.h"
#import "NXFavoriteTableViewCell.h"


@interface NXFavoriteViewController () <NXFavoriteTableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *favoriteDaraArray;

@end

@implementation NXFavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"NXFavoriteTableViewCell" bundle:nil] forCellReuseIdentifier:@"NXFavoriteTableViewCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:@"favoriteEdit" object:nil];
    
    self.favoriteDaraArray = [self getMutableSavedCity:kFavoriteKey];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateData
{
    self.favoriteDaraArray = [self getMutableSavedCity:kFavoriteKey];
    
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.favoriteDaraArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NXFavoriteTableViewCell *cell = [NXFavoriteTableViewCell new];
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"NXFavoriteTableViewCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NXFavoriteTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.delegate = self;
    
    cell.city = [self.favoriteDaraArray objectAtIndex:indexPath.row];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)saveCityArray:(NSMutableArray *)arrayData key:(id)key
{
    if (arrayData.count !=0) {
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayData];
        [[SettingsManagers instance] saveObject:data key:key];
    }
    else
    {
        [[SettingsManagers instance] saveObject:nil key:key];
    }
}

-(NSMutableArray *)getMutableSavedCity:(id)key
{
    NSMutableArray *result = [NSMutableArray new];
    
    if ([[SettingsManagers instance] objForKey:key]) {
        
        NSData *data = [[SettingsManagers instance] objForKey:key];
        
        NSArray *loadCity = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (loadCity.count !=0) {
            
            result = [[NSMutableArray alloc] initWithArray:loadCity];
        }
    }
    return result;
}

-(void)deleteFavorite:(City *)city
{
//    [self.favoriteDaraArray removeObject:city];
//    
//    [self saveCityArray:self.favoriteDaraArray key:kFavoriteKey];

    NSMutableArray *savedCityFavorite = [self getMutableSavedCity:kFavoriteKey];
    
    [savedCityFavorite enumerateObjectsUsingBlock:^(City *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.cityID isEqual:city.cityID] && [obj.countryID isEqual:city.countryID]) {
            
            [savedCityFavorite removeObject:obj];
            *stop = YES;
            return;
        }
        
    }];
    
    [self saveCityArray:savedCityFavorite key:kFavoriteKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"favoriteEdit" object:nil];
}

@end
