//
//  NXFavoriteTableViewCell.h
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"

@protocol NXFavoriteTableViewCellDelegate <NSObject>

-(void)deleteFavorite:(City *)city;

@end

@interface NXFavoriteTableViewCell : UITableViewCell

@property (nonatomic, weak) id <NXFavoriteTableViewCellDelegate> delegate;
@property (nonatomic, strong) City *city;
@property (strong, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *buttonDeleteFromFavorite;
- (IBAction)deleteFromFavoriteAction:(id)sender;

@end
