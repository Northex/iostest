//
//  NXFavoriteTableViewCell.m
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "NXFavoriteTableViewCell.h"

@implementation NXFavoriteTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteFromFavoriteAction:(id)sender {
    
    [self.delegate deleteFavorite:_city];
}

-(void)setCity:(City *)city
{
    _city = city;
    
    self.cityNameLabel.text = city.cityName;
    
}
@end
