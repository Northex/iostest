//
//  NXMainViewController.m
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "NXMainViewController.h"
#import "NXMainViewHeadTableViewCell.h"
#import "NXMainTableViewCell.h"
#import "Country.h"
#import "City.h"

static NSString *const kTableViewCellReuseIdentifier = @"TableViewCellReuseIdentifier";

@interface NXMainViewController () <NXMainTableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *countryDataArray;
@end

@implementation NXMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self getCountryData];

    [self.tableView registerNib:[UINib nibWithNibName:@"NXMainTableViewCell" bundle:nil] forCellReuseIdentifier:@"NXMainTableViewCell"];
    self.tableView.estimatedRowHeight = 64.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kTableViewCellReuseIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"NXMainViewHeadTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:kAccordionHeaderViewReuseIdentifier];
    // self.tableView.separatorColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
    self.tableView.allowMultipleSectionsOpen = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:@"favoriteEdit" object:nil];

}


-(void)updateData
{
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.countryDataArray.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    Country *country = [self.countryDataArray objectAtIndex:section];
    
    return country.citys.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kDefaultAccordionHeaderViewHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return [self tableView:tableView heightForHeaderInSection:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NXMainViewHeadTableViewCell *headCell = [NXMainViewHeadTableViewCell new];
    
    headCell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kAccordionHeaderViewReuseIdentifier];
    
    if (headCell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NXMainViewHeadTableViewCell" owner:self options:nil];
        headCell = [nib objectAtIndex:0];
        
    }
    
    headCell.country = [self.countryDataArray objectAtIndex:section];
    
    if ([self.tableView isSectionOpen:section]) {
        
        [headCell.imageViewArrow setImage:[UIImage imageNamed:@"arrowUp"]];
    }
    else
    {
        [headCell.imageViewArrow setImage:[UIImage imageNamed:@"arrowDown"]];
    }
    return headCell;
}




#pragma mark - <FZAccordionTableViewDelegate> -

- (void)tableView:(FZAccordionTableView *)tableView willOpenSection:(NSInteger)section withHeader:(NXMainViewHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowUp"]];
    
}

- (void)tableView:(FZAccordionTableView *)tableView didOpenSection:(NSInteger)section withHeader:(NXMainViewHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowUp"]];
    
}

- (void)tableView:(FZAccordionTableView *)tableView willCloseSection:(NSInteger)section withHeader:(NXMainViewHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowDown"]];
    
}

- (void)tableView:(FZAccordionTableView *)tableView didCloseSection:(NSInteger)section withHeader:(NXMainViewHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowDown"]];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NXMainTableViewCell *cell = [NXMainTableViewCell new];
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"NXMainTableViewCell"];
    
    if (cell == nil)
        
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NXMainTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.delegate = self;
    
    Country *country = [self.countryDataArray objectAtIndex:indexPath.section];
    
    City *city = [country.citys objectAtIndex:indexPath.row];
    

    
    NSMutableArray *savedCity = [self getMutableSavedCity:kCityKey];
    
    [savedCity enumerateObjectsUsingBlock:^(City *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.cityID isEqual:city.cityID] && [obj.countryID isEqual:city.countryID]) {
            
            city.isSelected = @"YES";
            *stop = YES;
            return;
        }
    }];
    
    NSMutableArray *savedCityFavorite = [self getMutableSavedCity:kFavoriteKey];
    
    if (savedCityFavorite.count !=0) {
     
        [savedCityFavorite enumerateObjectsUsingBlock:^(City *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([obj.cityID isEqual:city.cityID] && [obj.countryID isEqual:city.countryID]) {
                
                city.isFavorite = @"YES";
                *stop = YES;
                return;
            }
            
        }];

    }
    else
    {
        city.isFavorite = @"NO";
    }
    
        cell.city = city;
    
    if ([city.isSelected boolValue]) {
        
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    Country *selectedCountry = [self.countryDataArray objectAtIndex:indexPath.section];
    City *selectedCity = [selectedCountry.citys objectAtIndex:indexPath.row];
    
    NSMutableArray *savedCity = [self getMutableSavedCity:kCityKey];
    
    [savedCity addObject:selectedCity];
    
    [self saveCityArray:savedCity key:kCityKey];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Country *selectedCountry = [self.countryDataArray objectAtIndex:indexPath.section];
    City *selectedCity = [selectedCountry.citys objectAtIndex:indexPath.row];
    
    NSMutableArray *savedCity = [self getMutableSavedCity:kCityKey];
    
    [savedCity enumerateObjectsUsingBlock:^(City *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.cityID isEqual:selectedCity.cityID] && [obj.countryID isEqual:selectedCity.countryID]) {
            
            [savedCity removeObject:obj];
            *stop = YES;
            return;
        }
        
    }];
    
    [self saveCityArray:savedCity key:kCityKey];
}

-(void)getCountryData
{
    [[NXConnectionManager instance] getCountryWithParam:nil callback:^(id response, ErrorObj *error) {
        
        if (response) {
        
            [self parseDataFromServer:response[@"Result"]];
        }
        else
        {
            showError(error.message);
        }
    }];
}

-(void)saveCityArray:(NSMutableArray *)arrayData key:(id)key
{
    if (arrayData.count !=0) {
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayData];
        [[SettingsManagers instance] saveObject:data key:key];
    }
    else
    {
        [[SettingsManagers instance] saveObject:nil key:key];
    }
}

-(NSMutableArray *)getMutableSavedCity:(id)key
{
    NSMutableArray *result = [NSMutableArray new];
    
       if ([[SettingsManagers instance] objForKey:key]) {
    
           NSData *data = [[SettingsManagers instance] objForKey:key];
           
           NSArray *loadCity = [NSKeyedUnarchiver unarchiveObjectWithData:data];
           
           if (loadCity.count !=0) {
               
               result = [[NSMutableArray alloc] initWithArray:loadCity];
           }
       }
    return result;
}

-(void)parseDataFromServer:(NSArray *)data
{
    if (!self.countryDataArray) {
        
        self.countryDataArray = [NSMutableArray new];
    }
    
    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        Country *country = [[Country alloc] init];
        
        [country fillCountryFromDictinary:obj];
        
        [self.countryDataArray addObject:country];
        
    }];
    
    [self.tableView reloadData];
    
}

-(void)setFavorite:(City *)city
{
    [self.countryDataArray enumerateObjectsUsingBlock:^(Country *obj, NSUInteger idx, BOOL * _Nonnull stop) {
       
        if (obj.citys !=0) {
            
            [obj.citys enumerateObjectsUsingBlock:^(City *objCity, NSUInteger idx, BOOL * _Nonnull stop) {
                
                if ([city isEqual:objCity]) {
                    
                    if ([city.isFavorite boolValue]) {
                        
                        NSMutableArray *savedCityFavorite = [self getMutableSavedCity:kFavoriteKey];
                        
                        [savedCityFavorite addObject:city];
                        
                        [self saveCityArray:savedCityFavorite key:kFavoriteKey];
                    }
                    else
                    {
                        NSMutableArray *savedCityFavorite = [self getMutableSavedCity:kFavoriteKey];
                        
                        [savedCityFavorite enumerateObjectsUsingBlock:^(City *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            
                            if ([obj.cityID isEqual:city.cityID] && [obj.countryID isEqual:city.countryID]) {
                                
                                [savedCityFavorite removeObject:obj];
                                *stop = YES;
                                return;
                            }
                            
                        }];
                        
                        [self saveCityArray:savedCityFavorite key:kFavoriteKey];
                    }
                    
                }
            }];
        }
    }];
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"favoriteEdit" object:nil];
}


@end
