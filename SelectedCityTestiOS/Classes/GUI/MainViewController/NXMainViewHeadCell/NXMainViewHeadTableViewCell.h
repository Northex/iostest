//
//  NXMainViewHeadTableViewCell.h
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <FZAccordionTableView/FZAccordionTableView.h>
#import "Country.h"


static const CGFloat kDefaultAccordionHeaderViewHeight = 45.0f;
static NSString *const kAccordionHeaderViewReuseIdentifier = @"AccordionHeaderViewReuseIdentifier";

@interface NXMainViewHeadTableViewCell : FZAccordionTableViewHeaderView

@property (nonatomic, strong) Country              *country;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewArrow;
@property (strong, nonatomic) IBOutlet UILabel     *nameCountryLabel;

@end
