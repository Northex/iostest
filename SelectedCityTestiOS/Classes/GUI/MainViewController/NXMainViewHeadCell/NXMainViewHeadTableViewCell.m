//
//  NXMainViewHeadTableViewCell.m
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "NXMainViewHeadTableViewCell.h"

@implementation NXMainViewHeadTableViewCell

-(void)setCountry:(Country *)country
{
    _country = country;
    
    self.nameCountryLabel.text = country.countryName;
    
    [self.imageViewArrow setImage:[UIImage imageNamed:@"arrowUp"]];
    
}

@end
