//
//  NXMainTableViewCell.m
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "NXMainTableViewCell.h"

@implementation NXMainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UITapGestureRecognizer *selectFavorite = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectFavorite)];
    
    [self.favoriteImageView addGestureRecognizer:selectFavorite];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)selectFavorite
{
    if ([_city.isFavorite boolValue]) {
        
        _city.isFavorite = @"NO";
        
        [self.favoriteImageView setImage:[UIImage imageNamed:@"star"]];
        
    }
    else
    {
        [self.favoriteImageView setImage:[UIImage imageNamed:@"starSelected"]];
        
        _city.isFavorite = @"YES";
    }
    
    [self.delegate setFavorite:_city];
    
}
-(void)setCity:(City *)city
{
    _city = city;
    
    self.nameCityLabel.text = city.cityName;
    
    if ([city.isFavorite boolValue]) {
        
        [self.favoriteImageView setImage:[UIImage imageNamed:@"starSelected"]];
    }
    else
    {
        [self.favoriteImageView setImage:[UIImage imageNamed:@"star"]];
    }
    
    
}
@end
