//
//  NXMainTableViewCell.h
//  SelectedCityTestiOS
//
//  Created by Александр Сенченков on 17.01.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"

@protocol NXMainTableViewCellDelegate <NSObject>

-(void)setFavorite:(City *)city;

@end
@interface NXMainTableViewCell : UITableViewCell

@property (nonatomic, weak) id <NXMainTableViewCellDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIImageView *favoriteImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameCityLabel;
@property (nonatomic, strong) City *city;
@end
